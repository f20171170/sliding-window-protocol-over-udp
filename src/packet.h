#ifndef PACKET_H_INCLUDED
#define PACKET_H_INCLUDED

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <poll.h>
#include <semaphore.h>
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/random.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#if CHAR_BIT != 8
#error Unsupported platform. CHAR_BIT is not set to 8.
#endif

#define WINDOW_SIZE          5     /* The maximum number of unacknowledged packets that can exist on the server side (sliding window). */
#define PACKET_SIZE          100   /* Size of the packet's payload. Not the packet itself. */
#define PDR                  0.1   /* The packet drop rate. */
#define TIMEOUT_INTERVAL     2     /* The timeout interval for detecting lost packets specified in seconds. */
#define CLIENT_PORT_NO       9001  /* The port number of the client */
#define SERVER_PORT_NO       9002  /* The port number of the server. */
#define FIRST_RELAY_PORT_NO  9003  /* The port number of the first relay. */
#define SECOND_RELAY_PORT_NO 9004  /* The port number of the second relay. */
#define INPUT_FILENAME       "./docs/input_small.txt"
#define OUTPUT_FILENAME      "./output.txt"
#define CONSOLE_LOCK_SEM     "/CNAP2CS"       /* The name of the POSIX semaphore we'll be using to control access to the console. */
#define SERVER_BUFFER_SIZE   WINDOW_SIZE      /* The number of packets the server will buffer before it starts dropping packets.*/
#define PDB                  PDR * RAND_MAX   /* An integer value which can be used directly with the result of rand(). */

enum {PT_DATA, PT_ACK};
enum {NT_CLIENT, NT_SERVER, NT_RELAY1, NT_RELAY2};
enum {ET_SEND, ET_RECV, ET_DROP, ET_TIMEOUT, ET_RETRANSMISSION};
extern char *packet_type_str[2];  /* Defined in main.c */
extern char *node_type_str[4];    /* Defined in main.c */
extern char *event_type_str[5];   /* Defined in main.c */

typedef unsigned char byte_t;
typedef int fd_t;
typedef unsigned short int port_t;

typedef struct {
  unsigned int id;
  size_t size;
  unsigned short int type;
  unsigned long int seq_no;
  bool is_last;
  byte_t payload[PACKET_SIZE];
  unsigned short int source;  /* To quickly and simply indicate who the sender is. We could have just as easily used non-blocking recvfrom instead.*/
} Packet;

typedef struct {
  Packet packet;
  struct timeval timer;
  bool should_be_sent;
  bool was_ackd;
  bool timedout;
  bool sentinal;  /* This probably wasn't needed, but it made things easier, so we'll keep this field. */
} PacketContainer;

typedef PacketContainer Window[WINDOW_SIZE];

void clientMain();
void relayMain(unsigned short int relay_number);
void serverMain();
void scorchPG();
void die(char *filename, int line_no);
void logEvent(unsigned short int node_type, unsigned short int event_type, unsigned short int dest, Packet packet);
void resetTimer(struct timeval *timer);
bool timedOut(struct timeval *timer);
void seedRandomNumberGenerator();
bool shouldDropPacket(Packet input_packet);

#endif
