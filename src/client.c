#include "packet.h"

fd_t input_file, sock;
off_t input_file_ending;
bool input_file_completed = false;
char *CLIENT_FILENAME = "client.c";
extern struct sockaddr_in client_addr, relay_addrs[2], server_addr;

Packet getNextPacket();
void sendPacket(PacketContainer *packet_container);
struct timeval updateWindow(Window *window, fd_set *monitored_fds, struct timeval *elapsed_time);
void slideWindow(Window window);


/**
 * The entrypoint into the client's functionality. Handles fetching input,
 * writing to the relays, detecting dropped packets by maintaining timers,
 * maintaining a window size, etc.
*/
void
clientMain ()
{
  Window window;
  Packet ack_packet;
  int nfds, dest_id;
  fd_set monitored_fds;
  struct timeval start_time, end_time, elapsed_time, timeout, select_timeout;

  /* Initialize all of the timers. */
  resetTimer(&timeout);
  for (int i = 0; i < WINDOW_SIZE; ++i)
    window[i].timer = timeout;  /* We can reset all timers this way instead of repeated function calls. */
  
  /* First get a new socket from the OS. Since we're using UDP, this one socket
   * can be used for all outbound and inbound communication. We will have to bind
   * it though before we can receive any communications on it. */
  sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (sock == -1)
    die(CLIENT_FILENAME, __LINE__ - 2);
  if (bind(sock, (struct sockaddr *) &client_addr, sizeof(struct sockaddr_in)) == -1)
    die(CLIENT_FILENAME, __LINE__ - 1);

  /* Open the input file and figure out where it ends. */
  input_file = open(INPUT_FILENAME, O_RDONLY);
  if (input_file == -1)
    die(CLIENT_FILENAME, __LINE__ - 2);
  input_file_ending = lseek(input_file, 0, SEEK_END);
  lseek(input_file, 0, SEEK_SET);

  /* Fill each packet in the window, but don't send them yet. Just mark them as ready for sending. */
  for (int i = 0; i < WINDOW_SIZE; ++i) {
    window[i].packet = getNextPacket();
    window[i].should_be_sent = true;
    window[i].was_ackd = false;
    window[i].timedout = false;
  }

  /* Now we enter the main client loop. */
  while (!window[0].sentinal) {
    /* Send any packet that should be sent. The transmission delay (time taken for all
     * bits to be put on the wire/channel) here is negligible. */
    for (int i = 0; i < WINDOW_SIZE; ++i) {
      if (window[i].should_be_sent) {
        sendPacket(&window[i]);
        window[i].should_be_sent = false;
      }
    }

    /* Set up the monitored_fds set (for detecting ACKs). We're monitoring only one socket because
     * we're working with UDP, but we're expecting ACKs from 2 different sources (The relays). */
    FD_ZERO(&monitored_fds);
    FD_SET(sock, &monitored_fds);
    nfds = sock + 1;

    /* Perform a select call and calculate the elapsed time.
     * NOTE: While we're using a select call here, it really isn't needed since there is
     * only one socket that we are dealing with. But we use select here in the client
     * anyway because it is a cleaner way to specify and update the timeout rather than
     * use signals or non-blocking read with sleep. */
    gettimeofday(&start_time, NULL);
    select_timeout = timeout;
    if (select(nfds, &monitored_fds, NULL, NULL, &select_timeout) == -1)
      die(CLIENT_FILENAME, __LINE__ - 1);
    gettimeofday(&end_time, NULL);
    timersub(&end_time, &start_time, &elapsed_time);

    /* Determine if a packet was ack'd and if so, determine the seq_no. */
    if (FD_ISSET(sock, &monitored_fds)) {
      if (read(sock, &ack_packet, sizeof(Packet)) == -1)
        die(CLIENT_FILENAME, __LINE__ - 1);
      logEvent(NT_CLIENT, ET_RECV, NT_CLIENT, ack_packet);
    }

    /* Just a little logical assertion. */
    if (!(ack_packet.type == PT_ACK)) {
      fprintf(stderr, "%s %d: %s\n", CLIENT_FILENAME, __LINE__ - 1, "Client received a DATA packet where it was expecting an ACK.");
      scorchPG();
      exit(EXIT_FAILURE);
    }

    /* Do a linear search on the window to find the ack'd packet. Using a linear search
     * is actually not bad at all here since the window size is a small-ish constant.
     * Also, update every timer, packet, etc. accordingly. */
    for (int i = 0; i < WINDOW_SIZE; ++i) {
      if (window[i].packet.seq_no == ack_packet.seq_no) {
        window[i].was_ackd = true;
        window[i].should_be_sent = false;
        if (i == 0)
          slideWindow(window);
      }
      
      else if (!window[i].was_ackd && !window[i].sentinal) {
        timersub(&(window[i].timer), &elapsed_time, &(window[i].timer));
        if (timedOut(&(window[i].timer))) {
          dest_id = window[i].packet.id % 2 == 0 ? NT_RELAY1 : NT_RELAY2;
          logEvent(NT_CLIENT, ET_TIMEOUT, dest_id, window[i].packet);
          resetTimer(&(window[i].timer));
          window[i].timedout = true;
          window[i].should_be_sent = true;
        }
      }

      if (timercmp(&(window[i].timer), &timeout, <))
        timeout = window[i].timer;
    }

  }  /* End of while loop. */

  /* Send an ACK from the client to tell both the relays to exit. Assume that they go through and are
   * properly received (not dropped in between), we're not going to implement a closing handshake here
   * (though that would be a good idea - given more time, I'd implement it).
   * In some ways this is like a quick hack. */
  Packet end;
  end.id = 0;
  end.is_last = true;
  end.seq_no = 0;
  end.size = 0;
  end.source = NT_CLIENT;
  end.type = PT_ACK;
  sendto(sock, &end, sizeof(Packet), 0, (struct sockaddr *) &relay_addrs[0], sizeof(struct sockaddr_in));
  sendto(sock, &end, sizeof(Packet), 0, (struct sockaddr *) &relay_addrs[1], sizeof(struct sockaddr_in));

  close(input_file);
  close(sock);
}


/**
 * Create a packet for the next chunk of data based on the global_seq_no.
 * The input file must already be open before calling this function.
*/
Packet
getNextPacket ()
{
  int rd;
  Packet packet;
  static int id = 0;
  static unsigned long int seq_no = 0;

  rd = read(input_file, &packet.payload, PACKET_SIZE);
  if (rd == -1)
    die(CLIENT_FILENAME, __LINE__ - 2);

  packet.id = id;
  packet.size = rd;
  packet.type = PT_DATA;
  packet.source = NT_CLIENT;
  packet.seq_no = seq_no;

  if (seq_no + rd == input_file_ending) {
    packet.is_last = true;
    input_file_completed = true;
  } else {
    packet.is_last = false;
  }

  id += 1;
  seq_no += rd;

  return packet;
}


/**
 * Send the next specified packet (in the packet container) of data to the correct destination.
*/
void
sendPacket (PacketContainer *packet_container)
{
  int st;
  int dest_id;
  struct sockaddr_in dest_addr;

  if (packet_container->packet.id % 2 == 0) {
    dest_id = NT_RELAY1;
    dest_addr = relay_addrs[0];
  } else {
    dest_id = NT_RELAY2;
    dest_addr = relay_addrs[1];
  }

  st = sendto(sock, &packet_container->packet, sizeof(Packet), 0, (struct sockaddr *) &dest_addr, sizeof(struct sockaddr_in));
  if (st == -1)
    die(CLIENT_FILENAME, __LINE__ - 2);
  if (packet_container->timedout)
    logEvent(NT_CLIENT, ET_RETRANSMISSION, dest_id, packet_container->packet);
  else
    logEvent(NT_CLIENT, ET_SEND, dest_id, packet_container->packet);
}


/**
 * Slide a window as far as possible and fill the new slots with new packets.
 * The sliding algorithm is not the best since we are copying over in O(N) time,
 * but it's quite straightforward and bug-proof. Further, since N is actually a
 * fixed, small-ish constant, this shouldn't be an issue.
*/
void
slideWindow (Window window)
{
  int new_vacancies = 0;

  /* Find how many packets were ack'd in a row starting from the base of the window. */
  for (int i = 0; i < WINDOW_SIZE; ++i) {
    if (window[i].was_ackd)
      new_vacancies += 1;
    else
      break;
  }

  /* Shift the window and overwrite all of the packets that were not a part of the contiguously
   * ack'd block. */
  for (int i = 0, j = new_vacancies; j < WINDOW_SIZE ; ++i, ++j)
    window[i] = window[j];

  /* Fill in the new, empty slots at the end of the window. */
  for (int i = WINDOW_SIZE - new_vacancies; i < WINDOW_SIZE; ++i) {
    window[i].packet = getNextPacket();
    resetTimer(&(window[i].timer));
    window[i].was_ackd = false;
    window[i].timedout = false;
    if (window[i].packet.size == 0) {
      window[i].should_be_sent = false;
      window[i].sentinal = true;
    }
    else {
      window[i].should_be_sent = true;
      window[i].sentinal = false;
    }
  }
}

