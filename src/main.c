#include "packet.h"

sem_t *console_lock;
struct timeval origin_time;
char *MAIN_FILENAME = "main.c";
char *packet_type_str[2] = {"DATA", "ACK"};
char *node_type_str[4] = {"CLIENT", "SERVER", "RELAY1", "RELAY2"};
char *event_type_str[5] = {"S", "R", "D", "TO", "RE"};
struct sockaddr_in client_addr, relay_addrs[2], server_addr;


/**
 * Handle forking and kicking off separate proccesses for the client, the two relay nodes
 * and the server. The main advantage of this is that all of the processes can share the
 * same tty. But to prevent one write to the tty from colliding with another, we need to
 * use a global console lock (a mutex).
*/
int
main (int argc, char *argv[], char *envp[])
{
  int semval;
  int wstatus;
  port_t ports[4] = {CLIENT_PORT_NO, SERVER_PORT_NO, FIRST_RELAY_PORT_NO, SECOND_RELAY_PORT_NO};
  struct sockaddr_in *addrs[4] = {&client_addr, &server_addr, &relay_addrs[0], &relay_addrs[1]};

  /* Record the origin time. */
  gettimeofday(&origin_time, NULL);

  /* Set the process group. This will allow the scorched Earth policy of die to work later. */
  if (setpgid(0, 0) == -1)
    die(MAIN_FILENAME, __LINE__ - 1);

  /* open the semaphore (mutex) for controlling access to the console/tty. Create it if necessary.
   * If a semaphore already exists, we need to make sure that we set the the semaphore value back
   * to 1. */
  console_lock = sem_open(CONSOLE_LOCK_SEM, O_CREAT, O_RDWR | 0660, 1);
  sem_getvalue(console_lock, &semval);
  if (semval == 0)
    sem_post(console_lock);
  if (console_lock == SEM_FAILED)
    die(MAIN_FILENAME, __LINE__ - 2);
  
  /* Populate all of the address structures. */
  for (int i = 0; i < 4; ++i ) {
    addrs[i]->sin_family = AF_INET;
    addrs[i]->sin_port = htons(ports[i]);
    addrs[i]->sin_addr.s_addr = htonl(INADDR_ANY);
  }

  /* Print the headers for the event log using the format specified in the requirements. */
  printf("%-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s\n",
    "Node Name", "Event Type", "Timestamp", "Packet Type", "Packet ID", "Seq. No", "Source", "Dest");

  /* Spawn the client process. */
  if (fork() == 0) {
    clientMain();
    printf("Client has exited.\n");
    printf("File transfer complete!\n");
    return EXIT_SUCCESS;
  }

  /* Spawn the first relay process. */
  if (fork() == 0) {
    relayMain(1);
    printf("Relay 1 has exited.\n");
    return EXIT_SUCCESS;
  }

  /* Spawn the second relay process. */
  if (fork() == 0) {
    relayMain(2);
    printf("Relay 2 has exited.\n");
    return EXIT_SUCCESS;
  }

  /* Spawn the server process. */
  if (fork() == 0) {
    serverMain();
    printf("Server has exited.\n");
    return EXIT_SUCCESS;
  }

  /* Sort of like a join, expect we're dealing with multiple processes and not threads.
   * We need to wait because we don't want to return the shell prompt back to the user.
   * We won't, however, check the return status. */
  for (int i = 0; i < 4; ++i)
    wait(&wstatus);

  sem_close(console_lock);
  sem_unlink(CONSOLE_LOCK_SEM);
}


/** 
 * follow a scorched Earth policy: Kill every other process in the same process group before exiting.
*/
void
scorchPG ()
{
  fflush(stdout);
  fflush(stderr);
  if (killpg(getpgid(0), SIGTERM) == -1)
    fprintf(stderr, "%s %d: %s\n", MAIN_FILENAME, __LINE__ - 1, strerror(errno));
}


/**
 * Print the standard error message for the given errno then exit with a failing status code.
*/
__attribute__((noreturn))
void
die (char *filename, int line_no)
{
  fprintf(stderr, "%s %d: %s\n", filename, line_no, strerror(errno));
  scorchPG();
  exit(errno);
}


/**
 * Acquire a lock on the console.
*/
void
acquireConsoleLock ()
{
  if (sem_wait(console_lock) == -1)
    die(MAIN_FILENAME, __LINE__ - 1);
}


/**
 * Free the currently held lock on the console.
*/
void
releaseConsoleLock ()
{
  if (sem_post(console_lock) == -1)
    die(MAIN_FILENAME, __LINE__ - 1);
}


/**
 * Log an event in the event log without interfering with any other write to the same.
 * @param     node_type     The component that is performing the logging. A Node Type value, e.g. NT_CLIENT.
 * @param     event_type    The event that is being logged. An Event Type value, e.g. ET_RECV.
 * @param     dest          Who's supposed to be receiving the packet. A Node Type value.
 * @param     packet        The packet that we are logging an event for. We extract the packet type, id,
 *                          sequence number, and the source from this parameter.
*/
void
logEvent (unsigned short int node_type, unsigned short int event_type, unsigned short int dest, Packet packet)
{
  char *node_name;
  char timestamp[256];
  struct timeval current_time;
  gettimeofday(&current_time, NULL);
  timersub(&current_time, &origin_time, &current_time);
  node_name = node_type_str[node_type];
  sprintf(timestamp, "%ld.%ld", current_time.tv_sec, current_time.tv_usec);

  acquireConsoleLock();
  printf("%-20s %-20s %-20s %-20s %-20u %-20lu %-20s %-20s\n",
    node_name, event_type_str[event_type], timestamp, packet_type_str[packet.type],
    packet.id, packet.seq_no, node_type_str[packet.source], node_type_str[dest]);
  releaseConsoleLock();
}


/**
 * Set the timer back to the timeout value.
*/
void
resetTimer (struct timeval *timer)
{
  timer->tv_sec = TIMEOUT_INTERVAL;
  timer->tv_usec = 0;
}


/**
 * @returns     true if the packet's timer timed out, else false. 
*/
bool
timedOut (struct timeval *timer)
{
  return (timer->tv_sec < 0 || (timer->tv_sec == 0 && timer->tv_usec == 0));
}


/**
 * Seed the random number generator using /dev/urandom. See random(7) for more details.
 * Taken from the code from "Problem 1: File transfer using multi-channel stop-and-wait protocol"
 */
void
seedRandomNumberGenerator ()
{
  unsigned int seed;
  ssize_t bytes_read = getrandom(&seed, sizeof(unsigned int), 0);
  if (bytes_read == -1) {
    fprintf(stderr, "%d: Failed to initialize the random number generator. %s.\n", __LINE__ - 2, strerror(errno));
    exit(EXIT_FAILURE);
  }
  srand(seed);
}


/**
 * Determine if a packet should be dropped or not based on the PACK_DROP_RATE/PACKET_DROP_BOUNDRY.
 * Taken from the code from "Problem 1: File transfer using multi-channel stop-and-wait protocol"
 * @param     input_packet      The packet to test for. We don't really need this since we only
 *                              use this for DATA in the code. But we include it anyways for completeness.
 * @returns                     true if the packet should be dropped, otherwise false.
 */
bool
shouldDropPacket (Packet input_packet)
{
  unsigned int random_number = rand();
  if (input_packet.type == PT_ACK || random_number > PDB)
    return false;
  return true;
}

