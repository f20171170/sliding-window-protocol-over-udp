#include "packet.h"

fd_t sock, output_file;
char *SERVER_FILENAME = "server.c";
extern struct sockaddr_in client_addr, relay_addrs[2], server_addr;


/**
 * Write the contents of the buffer to the output file. Since the writes will
 * be sequential, there is no need to use lseek/fseek. Clean out the buffer
 * afterwards.
*/
void
flushBuffer (Packet packet_buffer[SERVER_BUFFER_SIZE])
{
  for (int i = 0; i < SERVER_BUFFER_SIZE; ++i) {
    if (write(output_file, packet_buffer[i].payload, packet_buffer[i].size) == -1)
      die(SERVER_FILENAME, __LINE__ - 1);
    memset(&packet_buffer[i], (int) '\0', sizeof(Packet));
  }
}


/**
 * Send an ack for the given packet.
 */
void
sendAck (Packet packet)
{
  int st, dest_id;
  struct sockaddr_in *return_addr;
  return_addr = packet.source == NT_RELAY1 ? &relay_addrs[0] : &relay_addrs[1];
  dest_id = packet.source;

  /* Just change some attributes of the received packet and send it back as an ACK. */
  packet.size = 0;
  memset(packet.payload, (int) '\0', PACKET_SIZE);
  packet.type = PT_ACK;
  packet.source = NT_SERVER;

  st = sendto(sock, &packet, sizeof(Packet), 0, (struct sockaddr *) return_addr, sizeof(struct sockaddr_in));
  if (st == -1)
    die(SERVER_FILENAME, __LINE__ - 2);
  logEvent(NT_SERVER, ET_SEND, dest_id, packet);
}



/**
 * Check if the packet falls inside the buffer window bounds. 
*/
bool
inBufferBounds (int packet_id, int lower_bound)
{
  int upper_bound = lower_bound + SERVER_BUFFER_SIZE;
  if (packet_id >= lower_bound && packet_id < upper_bound)
    return true;
  return false;
}


/**
 * The main entrypoint into the server's code.
*/
void
serverMain ()
{
  Packet packet;
  bool last_seen = false, terminal_condition = false;
  int buffer_min_id = 0, buffer_filled_amt = 0;
  Packet bounded_buffer[SERVER_BUFFER_SIZE];  /* We buffer the packet instead of just the payload to use headers like "size" later on. It's wiser to keep the packet. */
  bool packet_already_received[SERVER_BUFFER_SIZE] = {false};  /* Maybe we could have just reused the was_ackd field of packet containers... */

  /* Open a socket for all inbound and outbound communication. */
  sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (sock == -1)
    die(SERVER_FILENAME, __LINE__ - 2);
  if (bind(sock, (struct sockaddr *) &server_addr, sizeof(struct sockaddr_in)) == -1)
    die(SERVER_FILENAME, __LINE__ - 1);
  
  /* Open the output file for writing. */
  output_file = open(OUTPUT_FILENAME, O_WRONLY | O_CREAT | O_TRUNC, 0660);
  if (output_file == -1)
    die(SERVER_FILENAME, __LINE__ - 2);

  /* Now enter the main server loop. */
  while (true) {
    if (read(sock, &packet, sizeof(Packet)) == -1)
      die(SERVER_FILENAME, __LINE__ - 1);
    logEvent(NT_SERVER, ET_RECV, NT_SERVER, packet);

    if (inBufferBounds(packet.id, buffer_min_id)) {
      if (!packet_already_received[packet.id - buffer_min_id]) {
        buffer_filled_amt += 1;
        bounded_buffer[packet.id - buffer_min_id] = packet;
        packet_already_received[packet.id - buffer_min_id] = true;
        if (packet.is_last)
          last_seen = true;
      }

      if (last_seen) {
        terminal_condition = true;
        for (int i = 0; i < buffer_filled_amt; ++i) {
          if (bounded_buffer[i].size == 0) {
            terminal_condition = false;
            break;
          }
        }
      }

      if (buffer_filled_amt == SERVER_BUFFER_SIZE || terminal_condition) {
        flushBuffer(bounded_buffer);
        buffer_min_id += SERVER_BUFFER_SIZE;
        buffer_filled_amt = 0;
        for (int i = 0; i < SERVER_BUFFER_SIZE; ++i)
          packet_already_received[i] = false;
        if (terminal_condition) {
          /* We'll just assume that the client will receive this packet and exit. Ideally, we should
           * implement some kind of closing handshake to let both parties perform finalization tasks
           * before exiting. Sort of like TCP's closing handshake. */
          sendAck(packet);
          return;
        }
      }

      sendAck(packet);
    }
    
    else {
      logEvent(NT_SERVER, ET_DROP, NT_SERVER, packet);
    }

  }

  close(output_file);
  close(sock);
}

