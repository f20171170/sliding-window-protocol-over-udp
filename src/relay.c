#include "packet.h"

fd_t sock;
char *RELAY_FILENAME = "relay.c";
extern struct sockaddr_in client_addr, relay_addrs[2], server_addr;


/**
 * Add extra an extra simulated delay the packet for some random time between 0 and 2 ms.
*/
void
delayProcessing()
{
  int delay_time_ms = rand() % 3;
  if (delay_time_ms == 0)
    return;  /* Don't even bother making the function call. */
  usleep(delay_time_ms * 1000);
} 


/**
 * The entrypoint into the relay code. Each relay will handle reading data sent to it, introducing a
 * small delay, and then forwarding data. Then it directly forwards the server's ack back to the
 * client. The "relay" is basically a proxy. Unlike in the client we don't need to use an kind of
 * I/O multiplexing since there's only 1 socket and no need for timeouts.
*/
void
relayMain (unsigned short int relay_number)
{
  int st;
  Packet packet;
  struct sockaddr_in own_addr;
  unsigned long int packets_in = 0, packets_out = 0;
  unsigned short int original_packet_source, new_packet_source;
  
  if (relay_number == 1) {
    own_addr = relay_addrs[0];
    new_packet_source = NT_RELAY1;
  } else {
    own_addr = relay_addrs[1];
    new_packet_source = NT_RELAY2;
  }

  /* Each separate process must seed the random number generator with it's own seed. */
  seedRandomNumberGenerator();

  /* Open the socket for all inbound and outbound communication. */
  sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (sock == -1)
    die(RELAY_FILENAME, __LINE__ - 2);
  if (bind(sock, (struct sockaddr *) &own_addr, sizeof(struct sockaddr_in)) == -1)
    die(RELAY_FILENAME, __LINE__ - 1);

  /* The main loop for the relay. Exit after we've received the last packet
   * and the number of input and output packets are equal. */
  while (true) {
    if (read(sock, &packet, sizeof(Packet)) == -1)
      die(RELAY_FILENAME, __LINE__ - 1);

    logEvent(new_packet_source, ET_RECV, new_packet_source, packet);

    original_packet_source = packet.source;
    packet.source = new_packet_source;

    if (original_packet_source == NT_CLIENT) {
      if (packet.type == PT_ACK)
        break;  /* The client's way of letting the relays know that they can exit. */

      /* delay, maybe drop, and then send the packet to the server. */
      delayProcessing();

      if (shouldDropPacket(packet)) {
        logEvent(new_packet_source, ET_DROP, new_packet_source, packet);
        continue;
      }

      packets_in += 1;
      st = sendto(sock, &packet, sizeof(Packet), 0, (struct sockaddr *) &server_addr, sizeof(struct sockaddr_in));
      logEvent(packet.source, ET_SEND, NT_SERVER, packet);
      if (st == -1)
        die(RELAY_FILENAME, __LINE__ - 2);

    } else {

      /* Directly forword the ack back to the client. Just change the source of the packet. */
      packets_out += 1;
      st = sendto(sock, &packet, sizeof(Packet), 0, (struct sockaddr *) &client_addr, sizeof(struct sockaddr_in));
      logEvent(packet.source, ET_SEND, NT_CLIENT, packet);
      if (st == -1)
        die(RELAY_FILENAME, __LINE__ - 2);
    }
  }

  close(sock);
}

