# NOTE: This makefile has been made to be extremely simple. Since there are
# only a few source files, we do not generate intermediate object code and
# then link them later.

CC = gcc
SHELL = /bin/sh
CFLAGS = -Wall -g -lpthread
OUTPUT_FILENAME = "executable-program.elf"

all: src/packet.h src/main.c src/client.c src/relay.c src/server.c
	$(CC) src/main.c src/client.c src/relay.c src/server.c -o $(OUTPUT_FILENAME) $(CFLAGS)

clean:
	find -name $(OUTPUT_FILENAME) -delete
