# An Implementation of the Sliding Window Protocol Over UDP Sockets

This repository is a simple non-production case implemention of the [Sliding Window Protocol](https://en.wikipedia.org/wiki/Sliding_window_protocol) (or at least something quite close to it) to perform a file upload from a client over relays (basically proxies) to a server.

This was originally given as part of an assignment for Computer Networks (CS F303) at BITS Pilani in the Spring Semester of 2019-2020. This repository solves part 2 of the assignment (./docs/CS F303 Programming Assignment.pdf).

The code includes a good deal of inline comments to explain what's going on so I won't really need to explain that here. For the most part the code was written in a self-documenting way so it should be pretty easy to understand what's going on.

## Requirements:
The implementation was done in C (platform: GNU/Linux with GCC/glibc) and has been tested on the following platform:
- **OS:** Ubuntu 18.04.4 LTS x86_64
- **Kernel:** 4.15.0-96-generic
- **CPU:** Intel i7-8550U (8) @ 4.000GHz
- **Compiler:** gcc version 7.5.0 (Ubuntu 7.5.0-3ubuntu1~18.04)
- **Linker:** GNU ld (GNU Binutils for Ubuntu) 2.30
- **glibc Version:** Ubuntu GLIBC 2.27-3ubuntu1

One very important assumption made here is that all parts of the code will be compiled and run on the same platform. The main reason being that here we represent packets (headers + payload) as structs which may not be serialized and deserialized the same way across platforms (we don't implement or use a universal serialization scheme like GPB (Google protocol Buffers)).

